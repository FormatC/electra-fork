/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.compiler.gen;

import com.squareup.javapoet.JavaFile;

import java.io.IOException;

import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;

public abstract class AbstractCodeGenerator implements CodeGenerator {
	protected ProcessingEnvironment pe;
	protected Messager messager;
	protected Types typesUtils;
	protected Elements elementsUtils;


	public AbstractCodeGenerator(ProcessingEnvironment pe) {
		this.pe = pe;
		this.messager = pe.getMessager();
		this.typesUtils = pe.getTypeUtils();
		this.elementsUtils = pe.getElementUtils();
	}

	protected abstract JavaFile buildJavaFile();

	@Override
	public void generate() {
		JavaFile javaFile = buildJavaFile();

		try {
			javaFile.writeTo(pe.getFiler());
		} catch (IOException e) {
			messager.printMessage(Diagnostic.Kind.ERROR, "Cannot generate file: "
					+ javaFile.typeSpec.name
					+ ". Details : " + e.toString());
		}
	}
}
