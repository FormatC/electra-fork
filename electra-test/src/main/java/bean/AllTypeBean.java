package bean;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;

import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.annotation.Key;

@Entity
public class AllTypeBean {
    @Key
    private long id;
    private byte valueByte1;
    private Byte valueByte2;
    private int valueInt1;
    private Integer valueInt2;
    private long valueLong1;
    private Long valueLong2;
    private float valueFloat1;
    private Float valueFloat2;
    private double valueDouble1;
    private Double valueDouble2;

    private String valueStr;

    private boolean boolVal;

    private Date valueDate;
    private byte[] valueBlob;

    private BigInteger valueBI;
    private BigDecimal valueBD;

    private short valueShort1;
    private Short valueShort2;

    public byte getValueByte1() {
        return valueByte1;
    }

    public void setValueByte1(byte valueByte1) {
        this.valueByte1 = valueByte1;
    }

    public Byte getValueByte2() {
        return valueByte2;
    }

    public void setValueByte2(Byte valueByte2) {
        this.valueByte2 = valueByte2;
    }

    public int getValueInt1() {
        return valueInt1;
    }

    public void setValueInt1(int valueInt1) {
        this.valueInt1 = valueInt1;
    }

    public Integer getValueInt2() {
        return valueInt2;
    }

    public void setValueInt2(Integer valueInt2) {
        this.valueInt2 = valueInt2;
    }

    public long getValueLong1() {
        return valueLong1;
    }

    public void setValueLong1(long valueLong1) {
        this.valueLong1 = valueLong1;
    }

    public Long getValueLong2() {
        return valueLong2;
    }

    public void setValueLong2(Long valueLong2) {
        this.valueLong2 = valueLong2;
    }

    public float getValueFloat1() {
        return valueFloat1;
    }

    public void setValueFloat1(float valueFloat1) {
        this.valueFloat1 = valueFloat1;
    }

    public Float getValueFloat2() {
        return valueFloat2;
    }

    public void setValueFloat2(Float valueFloat2) {
        this.valueFloat2 = valueFloat2;
    }

    public double getValueDouble1() {
        return valueDouble1;
    }

    public void setValueDouble1(double valueDouble1) {
        this.valueDouble1 = valueDouble1;
    }

    public Double getValueDouble2() {
        return valueDouble2;
    }

    public void setValueDouble2(Double valueDouble2) {
        this.valueDouble2 = valueDouble2;
    }

    public String getValueStr() {
        return valueStr;
    }

    public void setValueStr(String valueStr) {
        this.valueStr = valueStr;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public byte[] getValueBlob() {
        return valueBlob;
    }

    public void setValueBlob(byte[] valueBlob) {
        this.valueBlob = valueBlob;
    }

    public BigInteger getValueBI() {
        return valueBI;
    }

    public void setValueBI(BigInteger valueBI) {
        this.valueBI = valueBI;
    }

    public BigDecimal getValueBD() {
        return valueBD;
    }

    public void setValueBD(BigDecimal valueBD) {
        this.valueBD = valueBD;
    }

    public boolean isBoolVal() {
        return boolVal;
    }

    public void setBoolVal(boolean boolVal) {
        this.boolVal = boolVal;
    }

    public short getValueShort1() {
        return valueShort1;
    }

    public void setValueShort1(short valueShort1) {
        this.valueShort1 = valueShort1;
    }

    public Short getValueShort2() {
        return valueShort2;
    }

    public void setValueShort2(Short valueShort2) {
        this.valueShort2 = valueShort2;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AllTypeBean that = (AllTypeBean) o;

        if (valueByte1 != that.valueByte1) return false;
        if (valueInt1 != that.valueInt1) return false;
        if (valueLong1 != that.valueLong1) return false;
        if (Float.compare(that.valueFloat1, valueFloat1) != 0) return false;
        if (Double.compare(that.valueDouble1, valueDouble1) != 0) return false;
        if (boolVal != that.boolVal) return false;
        if (valueShort1 != that.valueShort1) return false;
        if (valueByte2 != null ? !valueByte2.equals(that.valueByte2) : that.valueByte2 != null)
            return false;
        if (valueInt2 != null ? !valueInt2.equals(that.valueInt2) : that.valueInt2 != null)
            return false;
        if (valueLong2 != null ? !valueLong2.equals(that.valueLong2) : that.valueLong2 != null)
            return false;
        if (valueFloat2 != null ? !valueFloat2.equals(that.valueFloat2) : that.valueFloat2 != null)
            return false;
        if (valueDouble2 != null ? !valueDouble2.equals(that.valueDouble2) : that.valueDouble2 != null)
            return false;
        if (valueStr != null ? !valueStr.equals(that.valueStr) : that.valueStr != null)
            return false;
        if (valueDate != null ? !valueDate.equals(that.valueDate) : that.valueDate != null)
            return false;
        if (!Arrays.equals(valueBlob, that.valueBlob)) return false;
        if (valueBI != null ? !valueBI.equals(that.valueBI) : that.valueBI != null) return false;
        if (valueBD != null ? !valueBD.equals(that.valueBD) : that.valueBD != null) return false;
        return valueShort2 != null ? valueShort2.equals(that.valueShort2) : that.valueShort2 == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) valueByte1;
        result = 31 * result + (valueByte2 != null ? valueByte2.hashCode() : 0);
        result = 31 * result + valueInt1;
        result = 31 * result + (valueInt2 != null ? valueInt2.hashCode() : 0);
        result = 31 * result + (int) (valueLong1 ^ (valueLong1 >>> 32));
        result = 31 * result + (valueLong2 != null ? valueLong2.hashCode() : 0);
        result = 31 * result + (valueFloat1 != +0.0f ? Float.floatToIntBits(valueFloat1) : 0);
        result = 31 * result + (valueFloat2 != null ? valueFloat2.hashCode() : 0);
        temp = Double.doubleToLongBits(valueDouble1);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (valueDouble2 != null ? valueDouble2.hashCode() : 0);
        result = 31 * result + (valueStr != null ? valueStr.hashCode() : 0);
        result = 31 * result + (boolVal ? 1 : 0);
        result = 31 * result + (valueDate != null ? valueDate.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(valueBlob);
        result = 31 * result + (valueBI != null ? valueBI.hashCode() : 0);
        result = 31 * result + (valueBD != null ? valueBD.hashCode() : 0);
        result = 31 * result + (int) valueShort1;
        result = 31 * result + (valueShort2 != null ? valueShort2.hashCode() : 0);
        return result;
    }
}
