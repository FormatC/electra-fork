package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;
import org.bitbucket.txdrive.electra.annotation.Index;
import org.bitbucket.txdrive.electra.annotation.Schema;

@Entity(value = "CompIndex" , schema = {Schema.UPDATE},
		indexes = {@Index(value = "c3", columnNames = "name1, name3") })
public class CompIndexU extends CompIndex {
}
