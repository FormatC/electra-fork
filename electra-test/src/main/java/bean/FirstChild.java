package bean;

import org.bitbucket.txdrive.electra.annotation.Column;
import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity
public class FirstChild extends FirstSuperEntity {
	@Column("ACTIVE_CHILD") //override
	private String activeFlag;

	@Override
	public String getActiveFlag() {
		return activeFlag;
	}

	@Override
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
}
