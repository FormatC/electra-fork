package bean;

import org.bitbucket.txdrive.electra.annotation.Column;
import org.bitbucket.txdrive.electra.annotation.Key;
import org.bitbucket.txdrive.electra.annotation.SuperEntity;

@SuperEntity
abstract public class SecondSuperEntity extends FirstSuperEntity {
	@Column("ACTIVE_NEW") //Override
	private String activeFlag;

	@Key
	private String secondKey;

	@Override
	public String getActiveFlag() {
		return activeFlag;
	}

	@Override
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getSecondKey() {
		return secondKey;
	}

	public void setSecondKey(String secondKey) {
		this.secondKey = secondKey;
	}
}
