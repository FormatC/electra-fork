package bean;

import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity
public class SimpleTableToCreate {
	private String some;

	public String getSome() {
		return some;
	}

	public void setSome(String some) {
		this.some = some;
	}
}
