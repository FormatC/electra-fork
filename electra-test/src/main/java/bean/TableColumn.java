package bean;

import org.bitbucket.txdrive.electra.annotation.Column;
import org.bitbucket.txdrive.electra.annotation.Entity;

@Entity("CUSTOM_TABLE")
public class TableColumn {
	@Column("CUSTOM_NAME")
	private String name;
	@Column("CUSTOM_SUM")
	private float sum;

	public TableColumn() {
	}

	public TableColumn(String name, float sum) {
		this.name = name;
		this.sum = sum;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getSum() {
		return sum;
	}

	public void setSum(float sum) {
		this.sum = sum;
	}
}
