package org.bitbucket.txdrive.electra.core.manager;

import org.bitbucket.txdrive.electra.TestUtils;
import org.bitbucket.txdrive.electra.core.converter.ColumnType;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import bean.AllTypeBean;
import bean.Master;
import bean.TableInfo;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;


public class CreateTableTest extends AbstractRobolectricTest {
	public static final String TABLE_ALB_DEF = "CREATE TABLE 'AllTypeBean'('id' INTEGER PRIMARY KEY, 'valueByte1' INTEGER, 'valueByte2' INTEGER, 'valueInt1' INTEGER, 'valueInt2' INTEGER, 'valueLong1' INTEGER, 'valueLong2' INTEGER, 'valueFloat1' REAL, 'valueFloat2' REAL, 'valueDouble1' REAL, 'valueDouble2' REAL, 'valueStr' TEXT, 'boolVal' INTEGER, 'valueDate' INTEGER, 'valueBlob' BLOB, 'valueBI' TEXT, 'valueBD' TEXT, 'valueShort1' INTEGER, 'valueShort2' INTEGER)";
	public static final String SIMPLE_IDX1 = "CREATE INDEX 'MyIndex' ON 'SimpleIndex' (name)";
	public static final String SIMPLE_IDX2 = "CREATE INDEX 'SimpleIndex_name2' ON 'SimpleIndex' (name2)";
	public static final String COMPOSITE_IDX = "CREATE INDEX 'c1' ON 'CompIndex' (name1, name2)";
	public static final String COMPOSITE_IDX2 = "CREATE INDEX 'c2' ON 'CompIndex' (name3, name4)";
	public static final String PARENT = "CREATE TABLE 'CEntity'('id' INTEGER PRIMARY KEY, 'name' TEXT, 'field' INTEGER)";
	private static final String FIRST_CHILD_CREATE = "CREATE TABLE 'FirstChild'('id' INTEGER PRIMARY KEY, 'ACTIVE_CHILD' TEXT)";
	private static final String SECOND_CHILD_CREATE = "CREATE TABLE 'SecondChild'('id' INTEGER, 'ACTIVE_NEW' TEXT, 'secondKey' TEXT, 'name' TEXT, PRIMARY KEY(id, secondKey))";

	public void createAllTypeBeanTable() {
		List<TableInfo> tableInfoList = TestUtils.getTableInfo(em, AllTypeBean.class.getSimpleName());
		List<TableInfo> tableInfoListExpected = new ArrayList<>();
		tableInfoListExpected.add(new TableInfo("id", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueByte1", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueByte2", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueInt1", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueInt2", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueLong1", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueLong2", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueFloat1", ColumnType.REAL.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueFloat2", ColumnType.REAL.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueDouble1", ColumnType.REAL.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueDouble2", ColumnType.REAL.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueStr", ColumnType.TEXT.toString(), 0));
		tableInfoListExpected.add(new TableInfo("boolVal", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueDate", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueBlob", ColumnType.BLOB.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueBI", ColumnType.TEXT.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueBD", ColumnType.TEXT.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueShort1", ColumnType.INTEGER.toString(), 0));
		tableInfoListExpected.add(new TableInfo("valueShort2", ColumnType.INTEGER.toString(), 0));

		assertThat(tableInfoList, hasSize(tableInfoListExpected.size()));
		assertThat(tableInfoList, containsInAnyOrder(tableInfoListExpected.toArray()));

		Master tbl_name = TestUtils.getMasterForTable(em, AllTypeBean.class.getSimpleName());
		assertThat(tbl_name.getSql(), is(TABLE_ALB_DEF));
	}

	@Test
	public void createSimpleTable() {
		List<TableInfo> tableInfoList = TestUtils.getTableInfo(em,  "SIMPLE");
		List<TableInfo> tableInfoListExpected = new ArrayList<>();
		tableInfoListExpected.add(new TableInfo("CUSTOM_NAME", ColumnType.TEXT.toString(), 1));

		assertThat(tableInfoList, hasSize(tableInfoListExpected.size()));
		assertThat(tableInfoList, containsInAnyOrder(tableInfoListExpected.toArray()));
	}

	@Test
	public void createSimpleIndex() {
		Master index = TestUtils.getIndex(em, "SimpleIndex", "MyIndex");
		assertThat(index.getSql(), is(SIMPLE_IDX1));

		Master index2 = TestUtils.getIndex(em, "SimpleIndex", "SimpleIndex_name2");
		assertThat(index2.getSql(), is(SIMPLE_IDX2));
	}

	@Test
	public void createCompIndex() {
		Master ci1 = TestUtils.getIndex(em, "CompIndex", "c1");
		assertThat(ci1.getSql(), is(COMPOSITE_IDX));

		Master ci2 = TestUtils.getIndex(em, "CompIndex", "c2");
		assertThat(ci2.getSql(), is(COMPOSITE_IDX2));
	}

	@Test
	public void createTestEntityWithParent() {
		Master master = TestUtils.getMasterForTable(em, "CEntity");
		assertThat(master.getSql(), is(PARENT));
	}

	@Test
	public void createTestFirstChild() {
		Master master = TestUtils.getMasterForTable(em, "FirstChild");
		assertThat(master.getSql(), is(FIRST_CHILD_CREATE));
	}

	@Test
	public void createTestSecondChild() {
		Master master = TestUtils.getMasterForTable(em, "SecondChild");
		assertThat(master.getSql(), is(SECOND_CHILD_CREATE));
	}
}