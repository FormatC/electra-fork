package org.bitbucket.txdrive.electra.core.manager;

import org.bitbucket.txdrive.electra.core.exception.ElectraException;
import org.hamcrest.CoreMatchers;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bean.AllTypeBean;
import bean.FirstChild;
import bean.NoKey;
import bean.PKInt;
import bean.PKLong;
import bean.PKLongAuto;
import bean.PKMulti;
import bean.PKString;
import bean.SecondChild;

import static org.bitbucket.txdrive.electra.core.query.Restrictions.eq;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

public class CRUDTest extends AbstractRobolectricTest {

	@Test
	public void createAndRead() {
		AllTypeBean allTypeBean = setupAllTypeBean();
		long rowId = em.create(allTypeBean);
		assertThat(rowId, greaterThan(0L));
		assertThat(allTypeBean.getId(), greaterThan(0L));

		AllTypeBean retrieved = em.read(AllTypeBean.class, rowId);
		assertThat(retrieved.getId(), greaterThan(0L));
		assertThat(retrieved, is(notNullValue()));

		assertThat(retrieved, is(allTypeBean));
	}

	@Test
	public void createWithId() {
		AllTypeBean allTypeBean = setupAllTypeBean();
		long rowId = em.create(allTypeBean);
		long rowId2 = em.create(allTypeBean);
		assertThat(rowId2, not(is(rowId)));
	}

	@Test
	public void createAndReadNoKey() {
		NoKey noKey = new NoKey();
		noKey.setField("value");
		em.create(noKey);

		NoKey retrieved = em.select(NoKey.class).where(eq("field", "value")).first();
		assertThat(retrieved, is(notNullValue()));
		assertThat(retrieved.getField(), is("value"));
	}

	@Test
	public void createBulk() {
		List<AllTypeBean> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			AllTypeBean allTypeBean = setupAllTypeBean();
			list.add(allTypeBean);
		}
		boolean ok = em.create(list);
		assertThat(ok, is(true));

		for (AllTypeBean allTypeBean : list) {
			assertThat(allTypeBean.getId(), greaterThan(0L));
			AllTypeBean retrieved = em.read(AllTypeBean.class, allTypeBean.getId());
			assertThat(retrieved, is(allTypeBean));
		}

		assertThat((long)list.size(), is(em.select(AllTypeBean.class).func().rowCount()));
	}

	@Test
	public void readInvalidId() {
		AllTypeBean retrieved = em.read(AllTypeBean.class, -1);
		assertThat(retrieved, is(nullValue()));
	}

	@Test
	public void update() {
		AllTypeBean allTypeBean = setupAllTypeBean();
		long rowId = em.create(allTypeBean);
		assertThat(rowId, greaterThan(0L));

		updateValues(allTypeBean);
		boolean update = em.update(allTypeBean);
		assertThat(update, is(true));

		AllTypeBean retrieved = em.read(AllTypeBean.class, rowId);
		assertThat(retrieved, is(notNullValue()));
		assertThat(retrieved, is(allTypeBean));
	}

	@Test
	public void updateBulk() {
		List<AllTypeBean> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			AllTypeBean allTypeBean = setupAllTypeBean();

			em.create(allTypeBean);
			updateValues(allTypeBean);

			list.add(allTypeBean);
		}
		boolean ok = em.update(list);
		assertThat(ok, is(true));


		for (AllTypeBean allTypeBean : list) {
			assertThat(allTypeBean.getId(), greaterThan(0L));
			AllTypeBean retrieved = em.read(AllTypeBean.class, allTypeBean.getId());
			assertThat(retrieved, is(allTypeBean));
		}

		assertThat((long)list.size(), is(em.select(AllTypeBean.class).func().rowCount()));
	}

	@Test
	public void updateInvalidId() {
		AllTypeBean allTypeBean = new AllTypeBean();

		boolean update = em.update(allTypeBean);
		assertThat(update, is(false));
	}

	@Test(expected = ElectraException.class)
	public void updateNoKey() {
		NoKey noKey = new NoKey();
		em.create(noKey);

	    em.update(noKey);
	}


	@Test
	public void delete() {
		AllTypeBean allTypeBean = setupAllTypeBean();
		long rowId = em.create(allTypeBean);
		assertThat(rowId, greaterThan(0L));

		boolean delete = em.delete(allTypeBean);
		assertThat(delete, is(true));

		AllTypeBean retrieved = em.read(AllTypeBean.class, rowId);
		assertThat(retrieved, is(nullValue()));
	}

	@Test
	public void deleteBulk() {
		List<AllTypeBean> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			AllTypeBean allTypeBean = setupAllTypeBean();
			list.add(allTypeBean);
		}
		boolean ok = em.create(list);
		assertThat(ok, is(true));

		ok = em.delete(list);
		assertThat(ok, is(true));


		for (AllTypeBean allTypeBean : list) {
			AllTypeBean retrieved = em.read(AllTypeBean.class, allTypeBean.getId());
			assertThat(retrieved, is(nullValue()));
		}

		assertThat(em.func(AllTypeBean.class).rowCount(), is(0L));
	}


	@Test
	public void deleteInvalidId() {
		AllTypeBean allTypeBean = new AllTypeBean();
		boolean delete = em.delete(allTypeBean);
		assertThat(delete, is(false));
	}


	@Test(expected = ElectraException.class)
	public void deleteNoKey() {
		NoKey noKey = new NoKey();
		em.create(noKey);

		em.delete(noKey);
	}

	@Test
	public void save() {
		AllTypeBean allTypeBean = setupAllTypeBean();
		boolean save = em.save(allTypeBean);
		assertThat(save, is(true));
		assertThat(allTypeBean.getId(), greaterThan(0L));

		AllTypeBean retrieved = em.read(AllTypeBean.class, allTypeBean.getId());
		assertThat(retrieved.getId(), greaterThan(0L));
		assertThat(retrieved, is(notNullValue()));
		assertThat(retrieved, is(allTypeBean));

		updateValues(allTypeBean);
		save = em.save(allTypeBean);
		assertThat(save, is(true));

		retrieved = em.read(AllTypeBean.class, allTypeBean.getId());
		assertThat(retrieved.getId(), greaterThan(0L));
		assertThat(retrieved, is(notNullValue()));
		assertThat(retrieved, is(allTypeBean));
	}

	@Test(expected = ElectraException.class)
	public void saveNoKey() {
		NoKey noKey = new NoKey();
		em.save(noKey);
	}

	@Test
	public void exist() {
		AllTypeBean allTypeBean = setupAllTypeBean();
		assertThat(em.exists(AllTypeBean.class, 1L), is(false));
		long id = em.create(allTypeBean);
		assertThat(em.exists(AllTypeBean.class, id), is(true));
	}

	@Test
	public void existNoKey() {
		NoKey noKey = new NoKey();
		noKey.setField("value");
		em.create(noKey);
		assertThat(em.exists(NoKey.class, eq("field", "value")), is(true));
	}

	private void updateValues(AllTypeBean allTypeBean) {
		allTypeBean.setValueByte1((byte) 10);
		allTypeBean.setValueByte2((byte) 20);
		allTypeBean.setValueInt1(30);
		allTypeBean.setValueInt2(40);
		allTypeBean.setValueLong1(50L);
		allTypeBean.setValueLong2(60L);
		allTypeBean.setValueFloat1(70.5f);
		allTypeBean.setValueFloat2(70.7f);
		allTypeBean.setValueDouble1(80.5d);
		allTypeBean.setValueDouble2(80.7d);
		allTypeBean.setValueStr("TextUpdated");
		allTypeBean.setBoolVal(false);
		allTypeBean.setValueDate(new Date(new Date().getTime() + 1000));
		allTypeBean.setValueBI(new BigInteger("1000"));
		allTypeBean.setValueBD(new BigDecimal("2000.5"));
		allTypeBean.setValueShort1((short) 90);
		allTypeBean.setValueShort2((short) 100);
	}

	private AllTypeBean setupAllTypeBean() {
		AllTypeBean allTypeBean = new AllTypeBean();
		allTypeBean.setValueByte1((byte) 1);
		allTypeBean.setValueByte2((byte) 2);
		allTypeBean.setValueInt1(3);
		allTypeBean.setValueInt2(4);
		allTypeBean.setValueLong1(5L);
		allTypeBean.setValueLong2(6L);
		allTypeBean.setValueFloat1(7.5f);
		allTypeBean.setValueFloat2(7.7f);
		allTypeBean.setValueDouble1(8.5d);
		allTypeBean.setValueDouble2(8.7d);
		allTypeBean.setValueStr("Text");
		allTypeBean.setBoolVal(true);
		allTypeBean.setValueDate(new Date());
		allTypeBean.setValueBI(new BigInteger("100"));
		allTypeBean.setValueBD(new BigDecimal("200.5"));
		allTypeBean.setValueShort1((short) 9);
		allTypeBean.setValueShort2((short) 10);

		return allTypeBean;
	}

	@Test
	public void createPKLong() {
		PKLong pkLong = new PKLong();
		pkLong.setName("name");

		long rowId = em.create(pkLong);
		assertThat(rowId, greaterThan(0L));
		assertThat(pkLong.getId(), greaterThan(0L));

		PKLong retrieved = em.read(PKLong.class, pkLong.getId());
		assertThat(retrieved, is(notNullValue()));
	}

	@Test
	public void createPKString() {
		PKString entity = new PKString();
		entity.setKey("key");
		entity.setName("name");

		long rowId = em.create(entity);
		assertThat(rowId, greaterThan(0L));


		PKString retrieved = em.read(PKString.class, entity.getKey());
		assertThat(retrieved, is(notNullValue()));
	}

	@Test(expected = ClassCastException.class)
	public void createPKSInt() {
		PKInt entity = new PKInt();
		entity.setName("name");

		 em.create(entity);
	}

	@Test
	public void createPKLongAuto() {
		PKLongAuto entity = new PKLongAuto();
		entity.setName("name");

		long rowId = em.create(entity);
		assertThat(rowId, greaterThan(0L));
		assertThat(entity.getId(), greaterThan(0L));

		PKLongAuto retrieved = em.read(PKLongAuto.class, entity.getId());
		assertThat(retrieved, is(notNullValue()));
	}

	@Test
	public void PKMulti() {
		PKMulti entity = new PKMulti();
		entity.setId(1L);
		entity.setName("name");

		em.create(entity);

		PKMulti retrieved = em.read(PKMulti.class, entity.getId(), entity.getName());
		assertThat(retrieved, is(notNullValue()));
	}

	@Test
	public void testFirstChild() {
		FirstChild firstChild = new FirstChild();
		firstChild.setActiveFlag("Yes");
		em.create(firstChild);

		FirstChild loaded = em.read(FirstChild.class, firstChild.getId());
		assertThat(loaded, notNullValue());
		assertThat(loaded.getActiveFlag(), is(firstChild.getActiveFlag()));
	}

	@Test
	public void testSecondChild() {
		SecondChild secondChild = new SecondChild();
		secondChild.setActiveFlag("Yes");
		secondChild.setName("name");
		secondChild.setId(1L);
		secondChild.setSecondKey("key");
		em.create(secondChild);

		SecondChild loaded = em.read(SecondChild.class, secondChild.getId(), secondChild.getSecondKey());
		assertThat(loaded, notNullValue());
		assertThat(loaded.getActiveFlag(), is(secondChild.getActiveFlag()));
		assertThat(loaded.getName(), is(secondChild.getName()));
	}

	@Test
	public void testSecondChildWrongOrder() {
		SecondChild secondChild = new SecondChild();
		secondChild.setId(1L);
		secondChild.setActiveFlag("Yes");
		secondChild.setName("name");
		secondChild.setSecondKey("key");
		em.create(secondChild);

		SecondChild loaded = em.read(SecondChild.class, secondChild.getSecondKey(), secondChild.getId());
		assertThat(loaded, nullValue());
	}

	@Test(expected = ElectraException.class)
	public void testSecondChildInvalidKeySize() {
		SecondChild secondChild = new SecondChild();
		secondChild.setActiveFlag("Yes");
		secondChild.setName("name");
		secondChild.setId(1L);
		secondChild.setSecondKey("key");
		em.create(secondChild);

		SecondChild loaded = em.read(SecondChild.class, secondChild.getId());
		assertThat(loaded, notNullValue());
		assertThat(loaded.getActiveFlag(), is(secondChild.getActiveFlag()));
		assertThat(loaded.getName(), is(secondChild.getName()));
	}

	@Test(expected = ElectraException.class)
	public void testSecondChildInvalidKeySize2() {
		SecondChild secondChild = new SecondChild();
		secondChild.setActiveFlag("Yes");
		secondChild.setName("name");
		secondChild.setId(1L);
		secondChild.setSecondKey("key");
		em.create(secondChild);

		SecondChild loaded = em.read(SecondChild.class, 1, 2, 3);
		assertThat(loaded, notNullValue());
		assertThat(loaded.getActiveFlag(), is(secondChild.getActiveFlag()));
		assertThat(loaded.getName(), is(secondChild.getName()));
	}

	@Test(expected = ElectraException.class)
	public void testNoKeyRead() {
		NoKey nokey = new NoKey();
		nokey.setField("name");

		NoKey loaded = em.read(NoKey.class, 1);
	}
}
