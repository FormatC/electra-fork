/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.manager;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.txdrive.electra.core.exception.ElectraException;
import org.bitbucket.txdrive.electra.meta.IndexMeta;
import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.utils.SqlUtils;
import org.bitbucket.txdrive.electra.utils.StringUtils;

public class CreateIndexesOperation {

	private MetaType<?> metaType;
	private EntityManager em;

	public CreateIndexesOperation(MetaType<?> metaType, EntityManager manager) {
		this.metaType = metaType;
		this.em = manager;
	}

	public void execute() {
		for (IndexMeta index : getIndexes()) {
			String sql = "CREATE INDEX IF NOT EXISTS "
					+ SqlUtils.quote(index.getName())
					+ " ON " + SqlUtils.quote(metaType.getTableName())
					+ " (" + SqlUtils.csvValues(index.getColumnNames()) + ")";

			em.getDatabase().execSQL(sql);
		}
	}

	private List<IndexMeta> getIndexes() {
		List<IndexMeta> indexMetaList = new ArrayList<>();

		String tableName = metaType.getTableName();
		for (MetaField metaField : metaType.getMetaFields()) {
			IndexMeta index = metaField.getIndex();

			if (index == null) {
				continue;
			}

			String indexName = index.getName();

			if (StringUtils.isEmpty(index.getName())) {
				indexName = tableName + "_" + metaField.getColumnName();
			}

			indexMetaList.add(new IndexMeta(indexName, metaField.getColumnName()));
		}

		for (IndexMeta indexMeta : metaType.getIndexes()) {
			if (StringUtils.isEmpty(indexMeta.getName())) {
				throw new ElectraException("Index name cannot be empty for entity level definition");
			}

			if (indexMeta.getColumnNames() == null || indexMeta.getColumnNames().length == 0) {
				throw new ElectraException("Index column names cannot be empty for entity level definition");
			}

			indexMetaList.add(new IndexMeta(indexMeta.getName(), indexMeta.getColumnNames()));
		}


		return indexMetaList;
	}

}
