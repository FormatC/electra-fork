/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.manager;

import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.txdrive.electra.meta.MetaType;
import org.bitbucket.txdrive.electra.utils.SqlUtils;

public class DropIndexesOperation {
	private MetaType<?> metaType;
	private EntityManager em;

	public DropIndexesOperation(MetaType<?> metaType, EntityManager manager) {
		this.metaType = metaType;
		this.em = manager;
	}

	public void execute() {
		String talbeName = metaType.getTableName();

		for (String index : getIndexNames(talbeName)) {
			em.getDatabase().execSQL("DROP INDEX IF EXISTS " + SqlUtils.quote(index));
		}
	}

	private List<String> getIndexNames(String table) {
		List<String> names = new ArrayList<>();
		Cursor cursor = em
				.getDatabase()
				.rawQuery("SELECT NAME FROM SQLITE_MASTER WHERE TYPE = 'index' AND TBL_NAME = "
						+ SqlUtils.quote(table), null);

		if (cursor != null) {
			while (cursor.moveToNext()) {
				names.add(cursor.getString(0));

			}
			cursor.close();
		}

		return names;
	}

}
