/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.query;

import java.util.HashMap;

import org.bitbucket.txdrive.electra.core.exception.ElectraException;
import org.bitbucket.txdrive.electra.meta.MetaField;
import org.bitbucket.txdrive.electra.meta.MetaType;

public class QueryContext {
	private HashMap<String, String> names = new HashMap<>();

	public QueryContext(MetaType<?> metaType) {
		for (MetaField metaField : metaType.getMetaFields()) {
			names.put(metaField.getName(), metaField.getColumnName());
		}
	}

	public String resolveName(String name) {
		String resolvedName = names.get(name);

		if (resolvedName == null) {
			throw new ElectraException("Not found class field name: " + name);
		}

		return resolvedName;
	}

	public void validateName(String name) {
		if (names.get(name) == null) {
			throw new ElectraException("Not found class field name: " + name);
		}
	}

	public String resolveSql(String sql) {
		String result = sql;

		for (String name : names.keySet()) {
			result = result.replace(":" + name, names.get(name));
		}

		return result;
	}
}
