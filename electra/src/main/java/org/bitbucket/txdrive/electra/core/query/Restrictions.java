/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.core.query;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bitbucket.txdrive.electra.core.exception.ElectraException;
import org.bitbucket.txdrive.electra.utils.SqlUtils;

public final class Restrictions {
	private Restrictions() {
	}

	/**
	 * Apply an "and" constraint to the field
	 * @since 1.0.0
	 * @param expression expressions. 1..* expressions. If there is 1 expression,
	 * It will be used as single expression.
	 * @return Expression
	 */
	public static Expression and(final Expression... expression) {
		return new LogicalExpression(" AND ", expression);
	}

	/**
	 * Apply an "or" constraint to the field
	 * @since 1.0.0
	 * @param expression expressions. 1..* expressions. If there is 1 expression,
	 * It will be used as single expression.
	 * @return Expression
	 */
	public static Expression or(final Expression... expression) {
		return new LogicalExpression(" OR ", expression);
	}

	/**
	 * Apply an "equal" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param value The value to use in comparison
	 * @return Expression
	 */
	public static Expression eq(final String name, final Object value) {

		return (value == null) ?
				isNull(name) :
				new SimpleExpression(name, value, " = ");
	}

	/**
	 * Apply an "not equal" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param value The value to use in comparison
	 * @return Expression
	 */
	public static Expression neq(final String name, final Object value) {
		return (value == null) ?
				notNull(name) :
				new SimpleExpression(name, value, " <> ");
	}

	/**
	 * Apply an ("equal empty" or isNull) constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @return Expression
	 */
	public static Expression empty(final String name) {
		return or(isNull(name), eq(name, ""));
	}

	/**
	 * Apply an ("not equal empty" and notNull) constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @return Expression
	 */
	public static Expression notEmpty(final String name) {
		return and(notNull(name), neq(name, ""));
	}

	/**
	 * Apply an "Like %value%" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param value The value to use in comparison
	 * @return Expression
	 */
	public static Expression contains(final String name, final Object value) {
		return like(name, "%" + value + "%");
	}

	/**
	 * Apply an "like" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param value The value to use in comparison
	 * @return Expression
	 */
	public static Expression like(final String name, final Object value) {
		return new SimpleExpression(name, value, " LIKE ");
	}

	/**
	 * Apply an "greater than" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param value The value to use in comparison
	 * @return Expression
	 */
	public static Expression gt(final String name, final Object value) {
		return new SimpleExpression(name, value, " > ");
	}

	/**
	 * Apply an "less than" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param value The value to use in comparison
	 * @return Expression
	 */
	public static Expression lt(final String name, final Object value) {
		return new SimpleExpression(name, value, " < ");
	}

	/**
	 * Apply an "less than or equal" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param value The value to use in comparison
	 * @return Expression
	 */
	public static Expression lq(final String name, final Object value) {
		return new SimpleExpression(name, value, " <=");
	}

	/**
	 * Apply an "greater than or equal" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param value The value to use in comparison
	 * @return Expression
	 */
	public static Expression gq(final String name, final Object value) {
		return new SimpleExpression(name, value, " >= ");
	}

	/**
	 * Return the negation of an expression
	 * @since 1.0.0
	 * @param exp expression
	 * @return Expression
	 */
	public static Expression not(Expression exp) {
		return new NotExpression(exp);
	}

	/**
	 * Apply an "notnull" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @return Expression
	 */
	public static Expression notNull(String name) {
		return new NotNullExpression(name);
	}

	/**
	 * Apply an "isnull" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @return Expression
	 */
	public static Expression isNull(String name) {
		return new IsNullExpression(name);
	}

	/**
	 * Apply an "between" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param from the low value
	 * @param to the high value
	 * @return Expression
	 */
	public static Expression between(String name, Object from, Object to) {
		return new BetweenExpression(name, from, to);
	}

	/**
	 * Apply an "in" constraint to the field
	 * @since 1.0.0
	 * @param name field name
	 * @param values The values to use in comparison
	 * @return Expression
	 */
	public static Expression in(String name, Collection<?> values) {
		return new InExpression(name, values);
	}

	/**
	 * Apply sql expression. You can use db columns or field names.<BR>
	 * To use field names use the following format:   :field_name:<BR>
	 * Example: ":userName: = 'John'"
	 * @since 1.0.0
	 * @param sql sql expression
	 * @return Expression
	 */
	public static Expression sql(String sql) {
		return new SqlExpression(sql);
	}

	private static abstract class BaseExpression implements Expression {
		protected String name;
		protected String columnName;

		public BaseExpression(String name) {
			this.name = name;
		}

		@Override
		public void resolveNames(final QueryContext context) {
			if (columnName == null) {
				columnName = context.resolveName(name);
			}
		}
	}

	private static Expression wrapNull(SimpleExpression expression) {
		return expression.value == null ? isNull(expression.name) : expression;
	}

	private static class SimpleExpression extends BaseExpression {
		private Object value;
		private String operation;

		public SimpleExpression(String name, Object value, String operation) {
			super(name);
			this.value = value;
			this.operation = operation;
		}

		@Override
		public String toSql() {
			return columnName + operation + SqlUtils.getSQLValue(value);
		}
	}

	private static class LogicalExpression implements Expression {
		private Expression[] expression;
		private String operation;

		public LogicalExpression(String operation, Expression... expression) {
			this.expression = expression;
			this.operation = operation;
		}

		@Override
		public void resolveNames(QueryContext context) {
			for (Expression exp : expression) {
				exp.resolveNames(context);
			}
		}

		@Override
		public String toSql() {
			if (expression.length == 0) {
				throw new ElectraException("Logical expression has 0 items");
			}

			StringBuilder sql = new StringBuilder();

			if (expression.length == 1) {
				sql.append(expression[0].toSql());
			} else {
				sql.append("(");

				for (Expression exp : expression) {
					sql.append(exp.toSql()).append(operation);
				}

				sql.delete(sql.lastIndexOf(operation), sql.length());

				sql.append(")");
			}

			return sql.toString();
		}
	}


	private static class NotExpression implements Expression {
		private Expression expression;

		public NotExpression(Expression expression) {
			this.expression = expression;
		}

		@Override
		public String toSql() {
			return "NOT " + expression.toSql();
		}

		@Override
		public void resolveNames(QueryContext context) {
			expression.resolveNames(context);
		}
	}

	private static class NotNullExpression extends BaseExpression {
		public NotNullExpression(String name) {
			super(name);
		}

		@Override
		public String toSql() {
			return columnName + " NOTNULL";
		}

	}

	private static class IsNullExpression extends BaseExpression {
		public IsNullExpression(String name) {
			super(name);
		}

		@Override
		public String toSql() {
			return columnName + " ISNULL";
		}
	}

	private static class BetweenExpression extends BaseExpression {
		private Object from;
		private Object to;

		public BetweenExpression(String name, Object from, Object to) {
			super(name);
			this.from = from;
			this.to = to;
		}

		@Override
		public String toSql() {

			return columnName + " BETWEEN " +
					SqlUtils.getSQLValue(from) +
					" AND " +
					SqlUtils.getSQLValue(to);
		}
	}

	private static class InExpression extends BaseExpression {
		private List<String> stringValues = new ArrayList<>();

		public InExpression(String name, Collection<?> value) {
			super(name);

			for (Object v : value) {
				stringValues.add(SqlUtils.getSQLValue(v));
			}
		}

		@Override
		public String toSql() {
			return columnName + " IN ("
					+ SqlUtils.csvValues(
					stringValues.toArray(new String[stringValues.size()])) + ")";
		}
	}


	private static class SqlExpression implements Expression {
		private String sql;
		private String sqlResolved;

		public SqlExpression(String sql) {
			this.sql = sql;
		}

		@Override
		public void resolveNames(QueryContext context) {
			if (sqlResolved == null) {
				sqlResolved = context.resolveSql(sql);
			}
		}

		@Override
		public String toSql() {
			return sqlResolved;
		}
	}


}
