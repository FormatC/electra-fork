/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.meta;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.txdrive.electra.annotation.Schema;
import org.bitbucket.txdrive.electra.core.manager.ChangeType;
import org.bitbucket.txdrive.electra.core.manager.EntityListener;
import org.bitbucket.txdrive.electra.core.manager.EntityObservable;
import org.bitbucket.txdrive.electra.utils.StringUtils;

public class MetaType<T> implements EntityObservable<T> {
	private Class<T> type;
	private String name;
	private String tableName;
	private EntityCreator<T> creator;
	private EntityFilter<T> filter;
	private Schema[] schema = new Schema[]{};
	private List<MetaField<T, ?>> metaFields = new ArrayList<>();
	private List<IndexMeta> indexes = new ArrayList<>();

	private List<EntityListener<T>> entityListeners = new ArrayList<>();

	@Override
	public void addEntityListener(EntityListener<T> entityListener) {
		entityListeners.add(entityListener);
	}

	@Override
	public void removeEntityListener(EntityListener<T> entityListener) {
		entityListeners.add(entityListener);
	}

	@Override
	public void notifyPreCreate(T entity) {
		filter.preCreate(entity);
	}

	@Override
	public void notifyPostCreate(T entity) {
		filter.postCreate(entity);

		for (EntityListener<T> listener : entityListeners) {
			listener.onChange(ChangeType.CREATE, entity);
		}
	}

	@Override
	public void notifyPostRead(T entity) {
		filter.postRead(entity);
	}

	@Override
	public void notifyPreUpdate(T entity) {
		filter.preUpdate(entity);
	}

	@Override
	public void notifyPostUpdate(T entity) {
		filter.postUpdate(entity);

		for (EntityListener<T> listener : entityListeners) {
			listener.onChange(ChangeType.UPDATE, entity);
		}
	}

	@Override
	public void notifyPreDelete(T entity) {
		filter.preDelete(entity);
	}

	@Override
	public void notifyPostDelete(T entity) {
		filter.postDelete(entity);

		for (EntityListener<T> listener : entityListeners) {
			listener.onChange(ChangeType.DELETE, entity);
		}
	}

	@Override
	public void notifyChange(ChangeType changeType) {
		for (EntityListener<T> listener : entityListeners) {
			listener.onChange(changeType);
		}
	}

	public static class MetaTypeBuilder<T> {
		private MetaType<T> metaType = new MetaType<>();
		private List<MetaField<T, ?>> metaFields = new ArrayList<>();
		private List<IndexMeta> indexes = new ArrayList<>();

		public MetaTypeBuilder<T> withType(Class<T> type) {
			metaType.setType(type);
			return this;
		}

		public MetaTypeBuilder<T> withName(String name) {
			metaType.setName(name);
			return this;
		}

		public MetaTypeBuilder<T> withTableName(String tableName) {
			metaType.setTableName(tableName);
			return this;
		}

		public MetaTypeBuilder<T> withCreator(EntityCreator<T> creator) {
			metaType.setCreator(creator);
			return this;
		}

		public MetaTypeBuilder<T> withMetaField(MetaField<T, ?> metaField) {
			metaFields.add(metaField);
			return this;
		}

		public MetaTypeBuilder<T> withFilter(EntityFilter<T> filter) {
			metaType.setFilter(filter);
			return this;
		}

		public MetaTypeBuilder<T> withSchema(Schema... schema) {
			metaType.setSchema(schema);
			return this;
		}

		public MetaTypeBuilder<T> withIndex(IndexMeta indexMeta) {
			indexes.add(indexMeta);
			return this;
		}

		public MetaType<T> build() {
			metaType.setMetaFields(metaFields);
			metaType.setIndexes(indexes);
			return metaType;
		}
	}

	public Class<T> getType() {
		return type;
	}

	public void setType(Class<T> type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTableName() {
		if (StringUtils.isEmpty(tableName)) {
			tableName = name;
		}

		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public EntityCreator<T> getCreator() {
		return creator;
	}

	public void setCreator(EntityCreator<T> creator) {
		this.creator = creator;
	}

	public List<MetaField<T, ?>> getMetaFields() {
		return metaFields;
	}

	public void setMetaFields(List<MetaField<T, ?>> metaFields) {
		this.metaFields = metaFields;
	}

	public EntityFilter<T> getFilter() {
		return filter;
	}

	public void setFilter(EntityFilter<T> filter) {
		this.filter = filter;
	}

	public Schema[] getSchema() {
		return schema;
	}

	public void setSchema(Schema[] schema) {
		this.schema = schema;
	}

	public List<IndexMeta> getIndexes() {
		return indexes;
	}

	public void setIndexes(List<IndexMeta> indexes) {
		this.indexes = indexes;
	}
}
