/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.meta;

import org.bitbucket.txdrive.electra.core.exception.NoMetaTypeFound;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MetaTypes {
	private static Map<Class, MetaType> types;

	public static class ElectraMetaTypesBuilder {
		private List<MetaType> metaTypes = new ArrayList<>();

		public List<MetaType> build() {
			return metaTypes;
		}

		public ElectraMetaTypesBuilder withMetaType(MetaType metaType) {
			metaTypes.add(metaType);
			return this;
		}
	}

	public static void setTypes(Map<Class, MetaType> data) {
		types = data;
	}

	public static Map<Class, MetaType> getTypes() {
		return types;
	}

	public static <T> MetaType<T> typeOf(Class type) {
		if (types == null) {
			throw new NoMetaTypeFound();
		}

		MetaType<T> metaType = types.get(type);

		if (metaType == null) {
			throw new NoMetaTypeFound();
		}

		return metaType;
	}


}
