/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.utils;

import android.database.DatabaseUtils;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.txdrive.electra.core.exception.ElectraException;
import org.bitbucket.txdrive.electra.core.query.Expression;
import org.bitbucket.txdrive.electra.core.query.NameResolveAware;
import org.bitbucket.txdrive.electra.core.query.Property;
import org.bitbucket.txdrive.electra.core.query.QueryContext;
import org.bitbucket.txdrive.electra.core.query.Restrictions;
import org.bitbucket.txdrive.electra.meta.MetaField;

public class SqlUtils {

	public static String quote(String str) {
		return "'" + str + "'";
	}

	public static String toSqlLimit(long firstResult, long maxResult) {
		return (maxResult > 0) ? String.valueOf(firstResult) + "," + maxResult : null;
	}

	public static String getSQLValue(Object value) {

		return (value instanceof Number) ? String.valueOf(value)
				: DatabaseUtils.sqlEscapeString(String.valueOf(value));
	}

	public static String sqlCSV(QueryContext context, NameResolveAware[] sqlParts) {
		StringBuilder sql = new StringBuilder();

		for (NameResolveAware sqlPart : sqlParts) {
			sqlPart.resolveNames(context);
			sql.append(sqlPart.toSql()).append(", ");
		}

		sql.delete(sql.lastIndexOf(", "), sql.length());

		return sql.toString();
	}


	public static String sqlCSV(QueryContext context, String[] names) {
		StringBuilder sql = new StringBuilder();

		for (String name : names) {
			String columnName = context.resolveName(name);
			sql.append(columnName).append(", ");
		}

		sql.delete(sql.lastIndexOf(", "), sql.length());

		return sql.toString();
	}


	public static String csvValues(String[] values) {
		StringBuilder sql = new StringBuilder();

		for (String name : values) {
			sql.append(name).append(", ");
		}

		sql.delete(sql.lastIndexOf(", "), sql.length());

		return sql.toString();
	}

	public static boolean hasValues(Object[] values) {
		return values != null && values.length > 0;
	}

	public static <T> List<MetaField<T, ?>> filterByProperties(List<MetaField<T, ?>> metaFields, Property[] properties) {
		List<String> names = new ArrayList<>();

		for (Property property : properties) {
			if (names.contains("*")) {
				break;
			}

			String name = property.getName();
			if (name != null) {
				names.add(name);
			}
		}

		return names.contains("*") ? metaFields : getMetaFieldsByNames(metaFields, names);
	}

	public static Expression getExpressionByFields(Object entity, List<MetaField> fields) {
		Expression[] expressions = new Expression[fields.size()];

		int idx = 0;
		for (MetaField metaField : fields) {
			Object value = metaField.getAccessor().get(entity);
			expressions[idx] = Restrictions.eq(metaField.getName(), value);
			idx++;
		}

		return Restrictions.and(expressions);
	}

	public static Expression getExpression(List<MetaField> fields, Object... value) {
		Expression[] expressions = new Expression[fields.size()];

		int idx = 0;
		for (MetaField metaField : fields) {
			expressions[idx] = Restrictions.eq(metaField.getName(), value[idx]);
			idx++;
		}

		return Restrictions.and(expressions);
	}

	public static <T> List<MetaField<T, ?>> getMetaFieldsByNames(List<MetaField<T, ?>> metaFields
			, List<String> names) {
		List<MetaField<T, ?>> result = new ArrayList<>();

		for (MetaField<T, ?> metaField : metaFields) {
			if (names.contains(metaField.getName())) {
				result.add(metaField);
			}
		}

		return result;
	}

	public static <T> MetaField<T, ?> getMetaFieldByName(List<MetaField<T, ?>> metaFields
			, String name) {
		MetaField<T, ?> result = null;

		for (MetaField<T, ?> metaField : metaFields) {
			if (name.equals(metaField.getName())) {
				result = metaField;
				break;
			}
		}

		if (result == null) {
			throw new ElectraException("Field " + name + "not found");
		}

		return result;
	}


}
