/*
 * Copyright (C) 2016 Eugene Nadein
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.txdrive.electra.utils;

import java.util.List;

public class StringUtils {
	public static boolean isEmpty(String value) {
		return value == null || value.trim().equals("");
	}

	public static String join(List<String> values, String separator) {
		StringBuilder sql = new StringBuilder();

		for (String name : values) {
			sql.append(name).append(separator);
		}

		sql.delete(sql.lastIndexOf(separator), sql.length());

		return sql.toString();
	}

	public static String[] wrap(String[] values) {
		String[] result = new String[values.length];

		for (int i = 0; i < values.length; i++) {
			result[i] = "\"" + values[i] + "\"";
		}

		return result;
	}
}
